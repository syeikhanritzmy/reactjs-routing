import { useEffect, useState } from 'react';
import { Outlet, useParams } from 'react-router-dom';
import axios from 'axios';
export default function DetailPost() {
  let params = useParams();
  const [post, setPost] = useState({});

  useEffect(() => {
    const getOneData = async () => {
      const { data } = await axios.get(
        `https://jsonplaceholder.typicode.com/posts/${params.id}`
      );
      setPost(data);
    };
    return getOneData;
  }, []);
  return (
    <div className="container mx-auto">
      <h1 className="text-lg font-bold">Detail Post</h1>
      <h1 className="text-2xl">Title : {post.title}</h1>
      <p>sinopsis : {post.body}</p>
    </div>
  );
}
