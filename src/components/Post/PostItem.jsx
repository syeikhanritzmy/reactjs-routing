import React from 'react';
import { useState } from 'react';
import axios from 'axios';
import { useEffect } from 'react';
import { useNavigate, Outlet } from 'react-router-dom';
export default function PostItem() {
  const navigate = useNavigate();
  const [post, setPost] = useState([]);
  const detailPost = (id) => {
    navigate(`/detailpost/${id}`, { replace: true });
  };
  useEffect(() => {
    try {
      const getPost = async () => {
        const { data } = await axios.get(
          `https://jsonplaceholder.typicode.com/posts?_limit=20'`
        );
        console.log(data);
        setPost(data);
      };
      return getPost;
    } catch (error) {
      console.log(error);
    }
  }, []);
  return (
    <div className="flex gap-4 justify-center flex-wrap mt-2">
      {post.map((data) => (
        <div
          className="border-slate-200 border-2 w-56 h-72 flex flex-col justify-around"
          key={data.id}
        >
          <h2 className="font-bold text-lg pl-2 h-20">{data.title}</h2>

          <p className="text-xs text-justify p-2">
            {data.body.substr(0, 60)}...
          </p>
          <button
            className="bg-red-300 w-3/6 mx-auto h-10 font-medium"
            onClick={() => {
              detailPost(data.id);
            }}
          >
            Detail
          </button>
        </div>
      ))}
    </div>
  );
}
