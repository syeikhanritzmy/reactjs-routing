import React from 'react';
import { useNavigate, Link } from 'react-router-dom';
export default function Navbar() {
  let navigate = useNavigate();
  return (
    <>
      <div className="bg-blue-400 w-full h-20">
        <div className="container mx-auto flex items-center gap-x-5  h-full">
          <Link to="/" className="text-xl font-bold text-white">
            Home
          </Link>
          <Link to="/postpage" className="text-xl font-bold text-white">
            Posts
          </Link>
        </div>
      </div>
    </>
  );
}
