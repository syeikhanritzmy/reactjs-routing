import Navbar from './components/navbar';
import { Routes, Route } from 'react-router-dom';
import HomePage from './pages/home';
import PostPage from './pages/Posts';
import DetailPost from './components/Post/DetailPost';
function App() {
  return (
    <div className="App">
      <Navbar />
      <Routes>
        <Route path="/" element={<HomePage />} />
        <Route path="postpage/" element={<PostPage />}></Route>
        <Route path="detailpost/:id" element={<DetailPost />} />
      </Routes>
    </div>
  );
}

export default App;
