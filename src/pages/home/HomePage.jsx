import React from 'react';

export default function HomePage() {
  return (
    <div className="container mx-auto mt-3">
      <h1 className="font-bold text-3xl">HomePage</h1>
    </div>
  );
}
