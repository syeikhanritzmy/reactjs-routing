import React from 'react';
import PostItem from '../../components/Post';

export default function PostPage() {
  return (
    <div className="container mx-auto ">
      <PostItem />
    </div>
  );
}
